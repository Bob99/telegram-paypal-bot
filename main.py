from decimal import *
import re
from re import Match
from socket import error as SocketError
import sys
import time
import telebot
from telebot.types import Message

if len(sys.argv) < 3:
    print(f"A telegram bot calculating paypal taxes.\n\n"
          f"Usage:   {sys.argv[0]} <API_KEY> <ADMIN_CHAT_ID>")
    sys.exit()
API_KEY = sys.argv[1]
ADMIN_CHAT_ID = sys.argv[2]
bot: telebot.TeleBot = telebot.TeleBot(API_KEY)
last_requests_time: int = [0, 0, 0]
cooldown: int = 30

class LimitExceedError(Exception):
    def __init__(self, sleepTime):
        self.sleepTime = sleepTime

def main():
    run = True
    while run:
        try:
            bot.polling(interval=5)
        except LimitExceedError as e:
            time.sleep(e.sleepTime)
        except SocketError as e:
            print(f"connection error: {e.args}")
            run = False
        except telebot.apihelper.ApiTelegramException as e:
            print(f"Problem with API key: {e.args}")
            run = False


def calculate_amount(price: Decimal) -> Decimal:
    return (price + Decimal('0.35')) * (Decimal('100') / Decimal('97.51'))


def is_money_amount(s: str) -> bool:
    global last_requests_time
    now: int = int(time.time())
    if now - last_requests_time[0] < cooldown:
        bot.send_message(ADMIN_CHAT_ID, "Cooling down.")
        raise LimitExceedError(now-last_requests_time[0])

    last_requests_time.append(now)
    last_requests_time.pop(0)

    return ("€" in s) or ("$" in s) or ("eur" in s.lower())


@bot.message_handler(commands=["id"])
def show_id(msg: Message):
    bot.send_message(msg.chat.id, f"Your id is `{msg.chat.id}`", parse_mode="MarkdownV2")

@bot.message_handler(commands=["help", "start"])
def show_help(msg: Message):
    bot.send_message(msg.chat.id, "I'm a bot that helps with calculating for your PayPal transactions.\n\n"
                                  "Send me an amount of money (containing € or $) and I will calculate the amount you"
                                  "have to send via PayPal so that the receiver gets the full price.\n"
                                  "I calculate with a PayPal tax of *2.49% + 0.35 €/$*\n\n"
                                  "For example: Send me a message containing \"10€\"", parse_mode="Markdown")


@bot.message_handler(func=lambda msg: is_money_amount(msg.text))
def convert(msg: Message):
    price: Match = re.search(r"[0-9]+[,.]?[0-9]*[ $€EUR]*", msg.text.upper(), re.MULTILINE)
    mail: Match = re.search(r"(?<=\s)?\S+@\S+", msg.text, re.MULTILINE)
    subject: Match = re.search(r"für .*$", msg.text, re.MULTILINE)

    if price:
        unit: str = re.search(r"[$€EUR]+", price.group(0)).group(0)
        p = re.search(r"[0-9,.]+", price.group(0), re.MULTILINE)
        price: str = p.group(0).replace(',', '.')
        a: Decimal = calculate_amount(Decimal(price))
        amount: str = str(a.quantize(Decimal('.01')))
        bot.send_message(msg.chat.id, f"`{price}` {unit}  ⇒  `{amount}` {unit}", parse_mode="MarkdownV2")

    if mail:
        bot.send_message(msg.chat.id, f"\n{mail.group(0)}")

    if subject:
        bot.send_message(msg.chat.id, f"`{subject.group(0)}`", parse_mode="MarkdownV2")

    if not price and not mail and not subject:
        bot.reply_to(msg, "Sorry, I could not process this text :(")


@bot.message_handler()
def translate(msg: Message):
    bot.reply_to(msg, "Sorry, I could not process this text :(")


if __name__ == '__main__':
    main()
